# Nagios Plugins

Some custom(ized) Nagios plugins used on my projects

# Check for unread messages in GMail

Sometimes is useful to check that a specific folder in GMail have a limited amount of unread messages, e.g. to keep inbox clean, to be sure that some automation works correctly, and so on

This is taken from [this GitHub repository](https://github.com/geekpete/nagiosplugins) and improved

Usage is directly documented inside [the file itself](check_gmailunread.py)

